package io.gitlab.l_x_u.libarmor;

public interface ArmorTexture {

    public abstract String getAssetNamespace();
}
