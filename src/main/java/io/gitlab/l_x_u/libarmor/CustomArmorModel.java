package io.gitlab.l_x_u.libarmor;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;

public interface CustomArmorModel {

    @Environment(EnvType.CLIENT)
    public abstract <T extends LivingEntity> BipedEntityModel<T> getModel(EquipmentSlot slot);
}
