package io.gitlab.l_x_u.libarmor.client.render.model;

import net.minecraft.client.model.ModelPart;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.entity.LivingEntity;

// Example helmet model to demonstrate functionality
public class HelmetModel<T extends LivingEntity> extends BipedEntityModel<T> {

	public HelmetModel() {
		super(1F);
		textureWidth = 16;
		textureHeight = 16;
		this.head = new ModelPart(this);
		head.setPivot(0.0F, 0.0F, 0.0F);
		head.addCuboid(-8.0F, -16.0F, 0.0F, 16, 16, 1, 0.0F, false);
		head.addCuboid(-1.0F, -16.0F, -8.0F, 1, 16, 16, 0.0F, false);
		this.helmet = new ModelPart(this);
		helmet.setPivot(0.0F, 0.0F, 0.0F);
		helmet.addCuboid(-8.0F, -16.0F, 0.0F, 16, 16, 1, 0.0F, false);
		helmet.addCuboid(-1.0F, -16.0F, -8.0F, 1, 16, 16, 0.0F, false);
	}
}
