package io.gitlab.l_x_u.libarmor.mixin;

import com.google.common.collect.Maps;
import io.gitlab.l_x_u.libarmor.CustomArmorModel;
import io.gitlab.l_x_u.libarmor.ArmorTexture;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.ArmorFeatureRenderer;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.Map;

@Mixin(ArmorFeatureRenderer.class)
public abstract class ArmorFeatureRendererMixin<T extends LivingEntity> {

    private static final Map<String, Identifier> MOD_ARMOR_TEXTURE_CACHE = Maps.newHashMap();
    private static final String MODEL_MIXIN_TARGET = "Lnet/minecraft/client/render/entity/feature/ArmorFeatureRenderer;getArmor(Lnet/minecraft/entity/EquipmentSlot;)Lnet/minecraft/client/render/entity/model/BipedEntityModel;";

    // Custom model mixin
    @ModifyVariable(
            at = @At(
                    value = "INVOKE_ASSIGN",
                    target = MODEL_MIXIN_TARGET),
            method = "renderArmor"
    )
    private BipedEntityModel renderCustomModel(BipedEntityModel old, MatrixStack matrixStack, VertexConsumerProvider vertexConsumerProvider, LivingEntity livingEntity, float f, float g, float h, float i, float j, float k, EquipmentSlot equipmentSlot, int l) {

        Item equipped = livingEntity.getEquippedStack(equipmentSlot).getItem();
        if(equipped instanceof CustomArmorModel) {
            return ((CustomArmorModel) equipped).getModel(equipmentSlot);
        }
        return old;
    }

    // Custom asset directory mixin
    @Inject(
            at = @At("TAIL"),
            method = "getArmorTexture",
            locals = LocalCapture.CAPTURE_FAILHARD,
            cancellable = true
    )
    private void getCustomTexture(ArmorItem armorItem, boolean lowerParts, String suffix, CallbackInfoReturnable<Identifier> cir, String string) {

        if(armorItem instanceof ArmorTexture) {
            String namespace = ((ArmorTexture) armorItem).getAssetNamespace();
            Identifier ret = MOD_ARMOR_TEXTURE_CACHE.computeIfAbsent(string, s -> new Identifier(namespace, s));
            cir.setReturnValue(ret);
        }
    }
}
