package io.gitlab.l_x_u.libarmor.item

import io.gitlab.l_x_u.libarmor.Core
import io.gitlab.l_x_u.libarmor.CustomArmorModel
import io.gitlab.l_x_u.libarmor.ArmorTexture
import io.gitlab.l_x_u.libarmor.client.render.model.HelmetModel
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.client.render.entity.model.BipedEntityModel
import net.minecraft.entity.EquipmentSlot
import net.minecraft.entity.LivingEntity
import net.minecraft.item.ArmorItem
import net.minecraft.item.ArmorMaterial
import net.minecraft.item.Item

class AncientArmorItem(material: ArmorMaterial,  slot: EquipmentSlot, settings: Item.Settings) : CustomArmorModel,
    ArmorTexture, ArmorItem(material, slot, settings) {
    override fun getAssetNamespace(): String {
        return Core.MODID
    }

    @Environment(EnvType.CLIENT)
    override fun <T : LivingEntity?> getModel(slot: EquipmentSlot?): BipedEntityModel<T> {
        return HelmetModel()
    }
}