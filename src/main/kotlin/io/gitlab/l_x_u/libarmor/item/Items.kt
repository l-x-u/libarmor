package io.gitlab.l_x_u.libarmor.item

import io.gitlab.l_x_u.libarmor.AncientArmorMaterial
import net.minecraft.entity.EquipmentSlot
import net.minecraft.item.ArmorMaterial
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object Items {

    data class ArmorSet(val head: Item, val chest: Item, val legs: Item, val feet: Item)

    val ANCIENT_ARMOR_SET = create_armor_set(AncientArmorMaterial.ANCIENT, Item.Settings().group(ItemGroup.COMBAT))
    val ANCIENT_HAT inline get() = ANCIENT_ARMOR_SET.head
    val ANCIENT_SHIRT inline get() = ANCIENT_ARMOR_SET.chest
    val ANCIENT_PANTS inline get() = ANCIENT_ARMOR_SET.legs
    val ANCIENT_SHOES inline get() = ANCIENT_ARMOR_SET.feet

    fun init() {
        register_item("hat", ANCIENT_HAT)
    }

    fun register_item(name: String, item: Item) {
        Registry.register(Registry.ITEM, Identifier("inhabit", name), item)
    }

    fun create_armor_set(material: ArmorMaterial, settings: Item.Settings): ArmorSet {
        return ArmorSet(
            AncientArmorItem(material, EquipmentSlot.HEAD, settings),
            AncientArmorItem(material, EquipmentSlot.CHEST, settings),
            AncientArmorItem(material, EquipmentSlot.LEGS, settings),
            AncientArmorItem(material, EquipmentSlot.FEET, settings)
        )
    }
}