package io.gitlab.l_x_u.libarmor

import io.gitlab.l_x_u.libarmor.item.Items
import net.fabricmc.api.ModInitializer
import org.apache.logging.log4j.LogManager

// For support join https://discord.gg/v6v4pMv

object Core : ModInitializer {

    val MODID = "libarmor"

    val logger = LogManager.getLogger("LibArmor")

    override fun onInitialize() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.
        // Proceed with mild caution.
        logger.info("LibArmor Initializing...")
        Items.init()
    }
}
