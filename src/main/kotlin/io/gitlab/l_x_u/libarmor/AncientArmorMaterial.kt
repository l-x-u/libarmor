package io.gitlab.l_x_u.libarmor

import net.minecraft.entity.EquipmentSlot
import net.minecraft.item.ArmorMaterial
import net.minecraft.item.Items
import net.minecraft.recipe.Ingredient
import net.minecraft.sound.SoundEvent
import net.minecraft.sound.SoundEvents

enum class AncientArmorMaterial(private val repair_ingredient: Ingredient,
                                private val equip_sound: SoundEvent,
                                private val enchantability: Int,
                                private val toughness: Float,
                                private val durabilities: Map<EquipmentSlot, Int>,
                                private val protections: Map<EquipmentSlot, Int>,
                                private val nom: String) : ArmorMaterial {

    ANCIENT(Ingredient.ofItems(Items.BLAZE_ROD),
        SoundEvents.BLOCK_CONDUIT_ACTIVATE,
        15,
        0.5f,
        hashMapOf(
            EquipmentSlot.HEAD to 13,
            EquipmentSlot.CHEST to 15,
            EquipmentSlot.LEGS to 16,
            EquipmentSlot.FEET to 11
        ),
        hashMapOf(
            EquipmentSlot.HEAD to 3,
            EquipmentSlot.CHEST to 3,
            EquipmentSlot.LEGS to 3,
            EquipmentSlot.FEET to 3
        ),
        "ancient"
        );

    override fun getName(): String {
        return nom
    }

    override fun getRepairIngredient(): Ingredient {
        return repair_ingredient
    }

    override fun getToughness(): Float {
        return toughness
    }

    override fun getEquipSound(): SoundEvent {
        return equip_sound
    }

    override fun getDurability(slot: EquipmentSlot?): Int {
        return durabilities[slot]!!
    }

    override fun getEnchantability(): Int {
        return enchantability
    }

    override fun getProtectionAmount(slot: EquipmentSlot?): Int {
        return protections[slot]!!
    }
}