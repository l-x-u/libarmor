# Inhabit

## Development Tools

[Blockbench](https://blockbench.net/)
[Fabric Entity Model Mapper](https://github.com/UpcraftLP/FabricEntityModelMapper)

## Recommended Mods

### Optimizations

[Lithium](https://www.curseforge.com/minecraft/mc-mods/lithium)
[Phosphor](https://www.curseforge.com/minecraft/mc-mods/phosphor)
